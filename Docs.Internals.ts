class E{} // Widget (Private)
class ce extends E{} // Calendar Widget
class ue extends E{} // Clock
class le extends E{} // Dice
class Ie extends E{} // draw.v0
class Ee extends E{} // draw.v1
class ne extends E{} // embed
class Ce extends E{} // Group Maker
class he extends E{} // Image
class b extends E{} // Poll
class pe extends E{} // QR Code
class ge extends E{} // Random Name
class me extends E{} // sound level
class fe extends E{} // stopwatch
class se extends E{} // text editor
class oe extends E{} // Timer
class ve extends E{} // traffic light
class we extends E{} // Video
class ye extends E{} // webcam feed (Ye Olde Webcam:tm:)
class be extends E{} // Work Symbols
class Te {
    static getBoForType(t) {
        switch (t.type) {
            case "WIDGET_BACKGROUND_V1":
                throw new Error("Cannot instantiate background" );
            case "WIDGET_CALENDAR_V1":
                return ce;
            case "WIDGET_CLOCK_V1":
                return ue;
            case "WIDGET_DICE_V1":
                return le;
            case "WIDGET_DRAW_V0":
                return ca;
            case "WIDGET_DRAW_V1":
                return ua;
            case "WIDGET_EMBED_V1":
                return ne;
            case "WIDGET_GROUP_MAKER_V1":
                return At;
            case "WIDGET_IMAGE_V1":
                return he;
            case "WIDGET_POLL_V1":
                return j;
            case "WIDGET_QR_CODE_V1":
                return pe;
            case "WIDGET_RANDOM_NAME_V1":
                return ge;
            case "WIDGET_SOUND_LEVEL_V1":
                return me;
            case "WIDGET_STOPWATCH_V1":
                return fe;
            case "WIDGET_TEXT_V1":
                return Qe;
            case "WIDGET_TIMER_V1":
                return oe;
            case "WIDGET_TRAFFIC_LIGHT_V1":
                return ve;
            case "WIDGET_VIDEO_V1":
                return we;
            case "WIDGET_WEBCAM_V1":
                return ye;
            case "WIDGET_WORK_SYMBOLS_V1":
                return be;
            default:
                throw new Error("Unexaustive switch detected ("+t.type+")");
        }
    }
    static getNew(t) {
        return Te.getBoForType({ type: t.type }).getNew(t);
    }
    static getCachedOrEmpty(t) {
        return Te.getBoForType({ type: t.type }).getCachedOrEmpty(t);
    }
    static getCachedOrNull(t) {
        return Te.getBoForType({ type: t.type }).getCachedOrNull(t);
    }
    static getCachedOrError(t) {
        return Te.getBoForType({ type: t.type }).getCachedOrError(t);
    }
}


export {
    ce as C,
    ue as q,
    le as v,
    Ie,
    Ee,
    ne,
    Ce,
    he,
    b,
    pe,
    ge,
    me,
    fe,
    se,
    oe,
    ve,
    we,
    ye,
    be,
    Te
}


